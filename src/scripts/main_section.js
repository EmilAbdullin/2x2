import $ from 'jquery'
import 'slick-carousel';

$(function() {
  const defaultOptions = {
      infinite: false,
      draggable: false,
      focusOnSelect: false,
      accessibility: false,
      mobileFirst: false,
      dots: true,
      arrows: false,
      autoplay: false,
      autoplaySpeed: 6000,
      dotsClass: 'main-slide__dots',
    }

    // const arrowsOptions = {
    //   appendArrows: '.partners__arrows-wrapper',
    //   prevArrow: '<div class="arrow arrow_prev"></div>',
    //   nextArrow: '<div class="arrow arrow_next"></div>',
    // }

    // const sliderTabletOptions = {
    //   breakpoint: 767,
    //   settings: {
    //     dots: true,
    //     arrows: true,
    //     dotsClass: 'partners__dots',
    //     slidesToShow: 3,
    //     slidesToScroll: 2,
    //     ...defaultOptions,
    //     ...arrowsOptions
    //   }
    // }

    // const sliderDesktopOptions = {
    //   breakpoint: 991,
    //   settings: {
    //     arrows: true,
    //     dots: false,
    //     slidesToShow: 5,
    //     slidesToScroll: 5,
    //     ...defaultOptions,
    //     ...arrowsOptions
    //   }
    // }

    // const sliderOptions = {
    //   dots: true,
    //   dotsClass: 'partners__dots',
    //   arrows: false,
    //   slidesToShow: 2,
    //   slidesToScroll: 2,
    //   responsive: [
    //     sliderTabletOptions,
    //     sliderDesktopOptions
    //   ],
    //   ...defaultOptions
    // }

    $('#js-main-slider').slick(defaultOptions);
})